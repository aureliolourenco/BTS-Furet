import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;

public class App {

	public static void main(String[] args) {

		Fenetre fenetre = new Fenetre();
		JPanel aire = fenetre.getAire();

		List<Furet> furets = new ArrayList<>();
		Random r = new Random();
		
		for (int i = 0; i < 25; i++) {
			Point p = new Point(r.nextInt(400), r.nextInt(400));
			FuretCurieux furet = new FuretCurieux(p);
			furets.add(furet);
		}
		//placement des furets sur l'espace graphique
		for (Furet f : furets) {
			aire.add(f);
		}
		//***
		new Controller(fenetre, furets);
		
		//fenetre.setVisible(true);

	}

}
