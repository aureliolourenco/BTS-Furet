import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.FloatBuffer;
import java.util.List;

import javax.swing.JPanel;

public class Controller {

	Fenetre fenetre;
	List<Furet> furets;

	public Controller(Fenetre f, List<Furet> liste) {
		this(f);
		furets = liste;

	}

	public Controller(Fenetre f) {

		f.getAire().addMouseMotionListener(new MouseAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				Furet.cible = new Point(e.getX(), e.getY());
			}

		});

		f.getBtnGo().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				doJob();
			}
		});

		fenetre = f;
		f.setVisible(true);

	}

	private void doJob() {
		fenetre.getBtnGo().setEnabled(false);
		Mover m = new Mover(furets);
		m.start();

	}

}
