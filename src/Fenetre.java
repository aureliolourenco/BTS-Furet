import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Fenetre extends JFrame {

	private JPanel contentPane;
	private JTextField txtFuretCurieux;
	private JTextField txtFuretPeureux;
	private JPanel aire;
	private JButton btnGo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Fenetre frame = new Fenetre();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Fenetre() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 723, 578);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		aire = new JPanel();
		aire.setBackground(SystemColor.inactiveCaptionBorder);
		aire.setBounds(12, 12, 500, 500);
		contentPane.add(aire);
		aire.setLayout(null);
		
		txtFuretCurieux = new JTextField();
		txtFuretCurieux.setBounds(558, 27, 114, 19);
		contentPane.add(txtFuretCurieux);
		txtFuretCurieux.setColumns(10);
		
		txtFuretPeureux = new JTextField();
		txtFuretPeureux.setBounds(558, 54, 114, 19);
		contentPane.add(txtFuretPeureux);
		txtFuretPeureux.setColumns(10);
		
		btnGo = new JButton("GO");
		btnGo.setBounds(555, 119, 117, 25);
		contentPane.add(btnGo);
	}
	public JPanel getAire() {
		return aire;
	}
	public JTextField getTxtFuretCurieux() {
		return txtFuretCurieux;
	}
	public JTextField getTxtFuretPeureux() {
		return txtFuretPeureux;
	}
	public JButton getBtnGo() {
		return btnGo;
	}
}
