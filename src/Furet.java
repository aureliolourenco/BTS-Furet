import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

public abstract class Furet extends JPanel implements Movable {
	Point position;
	int taille = 20;
	Color couleur;
	int dx;
	int dy;
	
	static Point cible; 

	protected Furet() {

	}

	public Furet(Point p, Color c) {
		position = p;
		couleur = c;
		setLocation(p);
		setSize(taille, taille);
		setOpaque(false);
	}

	public Furet(Point p) {
		this(p, Color.RED);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(couleur);
		g.fillOval(0, 0, 10, 10);

	}

}
