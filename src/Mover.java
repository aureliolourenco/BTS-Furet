import java.util.List;

public class Mover implements Runnable {

	List<Furet> liste;
	Thread th;

	public Mover(List<Furet> l) {
		liste = l;
	}

	public void start() {
		th = new Thread(this);
		th.start();
	}

	public void stop() {
		th = null;
	}

	@Override
	public void run() {
		while(Thread.currentThread() == th) {
			//System.out.println("Running");
			for(Furet f : liste) {
				f.move();
			}
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
